# Based on Alpine docker image with LTS version of Node.js
# According to https://nodejs.org/en/about/releases/ the current LTS verion
# is v12 until 2022.
FROM node:lts-alpine
ARG PKGS=""
RUN sh -c "apk add $PKGS"
RUN yarn global add netlify-cli
